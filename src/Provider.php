<?php namespace pPort\Services\Url;
use pPort;
class Provider extends pPort\Provider {

    public $alias='url';

    public function register()
    {
    	 return "\\pPort\\Services\\Url\\UrlService";
    }
}